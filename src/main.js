import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import VueRouter from "vue-router";
import VueCookies from "vue-cookies";
import App from "@/App";
import VueI18n from "vue-i18n";

import Home from "@/views/Home";
import Register from "@/views/Register";
import Login from "@/views/Login";
import FightersList from "@/views/FightersList";
import Play from "@/views/Play";
import Account from "@/views/Account";
import UsersScores from "@/views/UsersScores";
import { authenticationStore } from "@/js/store/authentication-store";
import { powersRankingStore } from "@/js/store/powers-ranking-store";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

// Import Bootstrap an BootstrapVue CSS files (order is important)
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import messages from "./messages.json";

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(VueCookies);
Vue.use(VueI18n);

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);

Vue.use(VueAxios, axios);

axios.defaults.baseURL = "https://localhost:8000";

const routes = [
  { path: "/", component: Home, meta: { auth: false } },
  {
    path: "/register",
    component: Register,
    props: true,
    meta: { auth: false },
  },
  { path: "/login", component: Login, props: true, meta: { auth: false } },
  { path: "/fighters", component: FightersList, meta: { auth: true } },
  { path: "/play", component: Play, meta: { auth: true } },
  // { path: '/fighters/:pageIndex/:pageSize', component: FightersList, meta: { auth: true } },
  { path: "/account", component: Account, props: true, meta: { auth: true } },
  {
    path: "/users-scores",
    component: UsersScores,
    props: true,
    meta: { auth: true },
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

Vue.router = router;
App.router = Vue.router;

export default router;

axios.defaults.withCredentials = true;
Vue.use(VueAxios, axios);

const i18n = new VueI18n({
  locale: "en",
  messages,
});

Vue.config.productionTip = false;

new Vue({
  router,
  authenticationStore,
  powersRankingStore,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
