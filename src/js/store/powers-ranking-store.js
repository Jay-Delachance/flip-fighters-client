import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export const powersRankingStore = new Vuex.Store({
  state: {
    powersRanking: [],
  },
  plugins: [createPersistedState()],
  mutations: {
    update(state, powersRanking) {
      state.powersRanking = powersRanking;
    },
  },
  actions: {
    update({ commit, powersRanking }) {
      commit("update", powersRanking);
    },
  },
  getters: {
    getPowersRanking: (state) => {
      return state.powersRanking;
    },
  },
});
