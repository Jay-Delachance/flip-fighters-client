import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export const authenticationStore = new Vuex.Store({
  state: {
    authenticated: false,
  },
  plugins: [createPersistedState()],
  mutations: {
    login(state) {
      state.authenticated = true;
    },
    logout(state) {
      state.authenticated = false;
    },
  },
  getters: {
    isAuthenticated: (state) => {
      return state.authenticated;
    },
  },
});
